		</main> <!-- /#main-container -->
		<footer id="footer">

			<?php $footer_menu = nicdao_get_menu_items('footer'); ?>
			<ul id="footer-menu">
			<?php foreach ($footer_menu as $menu_item): ?>
				<li>
					<a class="btn btn-default btn-border" href="<?php echo $menu_item->url; ?>">
						<?php echo $menu_item->title; ?>
					</a>
				</li>
			<?php endforeach; ?>
			</ul>

	    <div class="container text-center">

				<?php if(file_exists(get_template_directory().'/0_img/logo.svg')):  ?>
				  <?php $logo_svg = file_get_contents(get_bloginfo('template_directory').'/0_img/logo.svg'); echo $logo_svg; ?>
				<?php else: bloginfo('name'); endif; ?>

				<p><?php the_field('footer_contact_details', 'options'); ?></p>

				<span class="attribution">Website by <a href="http://worboysdesign.com" target="_blank">Worboys Design</a></span>

	    </div>

			<?php echo getSocialLinksHTML(); ?>

		</footer>
	</div> <!-- /#wrap -->
	<?php wp_footer(); ?>
</body>
</html>
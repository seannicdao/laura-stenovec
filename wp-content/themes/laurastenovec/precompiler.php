<?php
	$precompiler_on = array(
		'haml' => false,
		'jade' => false,
		'coffeescript' => false,
		'scss' => true,
	);

	$libs_dir =  get_template_directory().'/0_phplibs/';
	foreach (glob($libs_dir.'*.php') as $filename) {
		require_once $filename;
	}

	if ($sntk) {
		$compiler = $sntk->getCompiler();
	

		//Jade Compiler
		if ($precompiler_on['jade']) {
			$source_dir = get_template_directory().'/0_jade/templates';
			$destination_dir = get_template_directory()."/";
			if ($compiler->compile_dir($source_dir, $source_dir, $destination_dir)) {
				header("Location: http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
				exit();
			}
		}

		//HAML Compiler
		if ($precompiler_on['haml']) {
			$source_dir = get_template_directory().'/';
			$destination_dir = app_path()."/views/";
			if ($compiler->compile_dir($source_dir, $source_dir, $destination_dir)) {
				header("Location: http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
				exit();
			}
		}

		//CoffeeScript Compiler
		if ($precompiler_on['coffeescript']) {
			$source_dir = app_path()."/assets/coffee/*";
			$destination_dir = public_path()."/js/";
			if ($compiler->compile_dir($source_dir, $source_dir, $destination_dir)) {}
		}

		//SCSS Compiler
		if ($precompiler_on['scss']) {
			$source_dir = get_template_directory().'/0_scss/';
			$destination_dir = get_template_directory().'/0_css/';
			if ($compiler->compile_dir($source_dir, $source_dir, $destination_dir)) {}
		}
	}
?>

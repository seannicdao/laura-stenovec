<?php echo getSocialLinksHTML(); ?>
<div class="container">
  <div class="navbar navbar-toggleable-md">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-bars"></i>
    </button>
    <a class="navbar-brand" href="<?php echo home_url(); ?>">
      <?php if(file_exists(get_template_directory().'/0_img/logo.svg')):  ?>
        <?php $logo_svg = file_get_contents(get_bloginfo('template_directory').'/0_img/logo.svg'); echo $logo_svg; ?>
      <?php else: bloginfo('name'); endif; ?>
    </a>

    <?php
      wp_nav_menu( array(
        'menu'              => 'primary',
        'theme_location'    => 'primary',
        'depth'             => 2,
        'container'         => 'nav',
        'container_class'   => 'collapse navbar-collapse',
        'container_id'      => 'navbarSupportedContent',
        'menu_class'        => 'navbar-nav',
        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
        'walker'            => new wp_bootstrap_navwalker(),
      ));
    ?>
  </div>
</div>
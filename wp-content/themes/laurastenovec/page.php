<?php

  get_header();
  while(have_posts()): the_post();
?>
<?php
  $has_billboard = get_field('use_billboard');
  if ($has_billboard) {
    $billboard_image = get_field('billboard_image');
    $billboard_content = get_field('billboard_content');
    $billboard_settings = get_field('billboard_settings');

    $billboard = new Billboard();
    $billboard->setImage($billboard_image['sizes']['billboard']);
    $billboard->setContent($billboard_content);
    $billboard->setMeta('classes', $billboard_settings);
    $billboard->render();
  }

  $content = get_the_content();

?>

<?php if ($content): ?>

<div id="page-content" class="section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>

        <?php wp_link_pages(array(
          'before' => '<div>',
          'after' => '</div>',
          'next_or_number'   => 'next',
          'nextpagelink'     => __( 'Next' ),
          'previouspagelink' => __( 'Previous' ),
        )); ?>
      </div>
    </div>
  </div>
</div>

<?php endif; ?>

<?php
  if ($sections = get_field('sections')) {
    foreach ($sections as $i => $section) {
      $section = new Section($section);
      $section->index = $i;
      $section->post_id = $post->ID;
      $section->render();
    }
  }
?>
<?php endwhile; ?>
<?php get_footer(); ?>
<?php

class Square {
  public $title;
  public $img;
  public $url;

  public function __construct( $title = '', $img = false, $url = false ) {
    $this->title = $title;
    $this->img = $img;
    $this->url = $url;
  }

  private function classesArray() {
    $classes = array();
    $classes[] = $this->img ? 'has-img' : false;
    $classes[] = $this->url ? 'has-url' : false;
    return $classes;
  }

  public function render() {
    $square = $this;

    $classes_array = $this->classesArray();

    $filepath = PARTIALS_DIR.'/square.php';

    if (file_exists($filepath)) {
      include $filepath;
      return true;
    }

    return false;
  }
}
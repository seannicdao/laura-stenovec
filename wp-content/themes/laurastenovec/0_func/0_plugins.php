<?php

  $theme_plugin_dir = '/0_func/plugins/';

  $plugins = array(
    // 'ACF' => 'acf/acf.php',
  );

  foreach ($plugins as $plugin_name => $plugin_path) {
    $full_plugin_path = get_template_directory().$theme_plugin_dir.$plugin_path;
    include $full_plugin_path;
  }

?>
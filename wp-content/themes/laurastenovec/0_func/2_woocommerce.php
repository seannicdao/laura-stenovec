<?php

	remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
	remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
	remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

	add_action('woocommerce_before_main_content', 'sn_woocommerce_output_content_wrapper', 10);
	add_action('woocommerce_after_main_content', 'sn_woocommerce_output_content_wrapper_end', 20);

	function sn_woocommerce_output_content_wrapper() {
		echo '<div class="section">';
		echo '<div class="container">';
	}

	function sn_woocommerce_output_content_wrapper_end() {
		echo '</div>';
		echo '</div>';
	}

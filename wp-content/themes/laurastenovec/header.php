<?php

  if (file_exists(__DIR__.'/precompiler.php')) {
    $sntk = &$GLOBALS['sntk'];
    require_once("precompiler.php");
  } else {
  }

  require_once("global_POST.php");
  
?>
<!DOCTYPE html>
<html>
<head>
  <title><?php bloginfo('name'); ?><?php if ($post) { echo ' | '.get_the_title(); } ?></title>
  <?php
    //<!-- meta - generic -->
    require_once('meta.php');
    //<!-- wordpress header hook -->
    wp_head();
    //<!-- fonts -->
    require_once('fonts.php');
    //<!-- css -->
    require_once('css.php');
    //<!-- global_vars -->
    require_once('global-vars.php');
    //<!-- libs -->
    require_once('libs.php');
    //<!-- func -->
    //require_once('0_func_pre.php');
    //<!-- scripts -->
    //require_once('0_scripts_pre.php');
  ?>


  <?php if (!is_user_logged_in()): ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120655878-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-120655878-1');
    </script>
  <?php endif; ?>
  
</head>
<?php
  if (is_page()) {
    $body_classes[] = 'page-'.$post->post_name;
  }
  if (is_category()) {
    $category = $wp_query->get_queried_object();
    $body_classes[] = 'category-'.$category->slug;
  }
  if (get_field('use_billboard')) {
    $body_classes[] = 'has-billboard';
  }
?>
<body <?php body_class(implode(' ', $body_classes)); ?>>

<div id="loading">
  <i></i>
</div>

<?php include PARTIALS_DIR.'/modal.php'; ?>
<?php include PARTIALS_DIR.'/account/modal-login-content.php'; ?>

<div id="wrap">
  <div id="header" class="clearfix">
    <?php require_once('navwalker.php'); ?>
  </div>
  <main id="main-container">
<?php

	$wedding_date = 'Uh oh, the wedding date hasn\'t been set!';

	$deadline = get_field('wedding_date', 'options');
	if ($deadline) {
		$deadline = strtotime($deadline);
		$wedding_date = date('F jS, Y',$deadline);
	}

?>

<span class="content-wedding-date">
	<?php echo $wedding_date; ?>
</span>

<?php



?>

<div class="row row-people">
<?php foreach($content->people as $person): ?>
	<div class="col col-12 col-md-6 col-lg-4">
		<?php
			$person = new Person($person);

			$square = new Square($person->name, $person->getImgUrl('large'), array(
				'target' => 'modal',
				'title' => $person->name,
				'content' => $person->description,
				'cta-url' => 'mailto:'.$person->email,
				'cta-text' => 'Contact',
			));

			// var_dump($square);

			$square->render();
		?>
	</div>
<?php endforeach; ?>
</div>

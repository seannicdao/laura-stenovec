<?php

	$classes = array();

	$classes[] = (isset($content->center) && $content->center) ? 'text-center' : '';

?>

<h2 class="section-title <?php echo implode(' ', $classes); ?>">
	<span><?php echo $content->title; ?></span>
</h2>

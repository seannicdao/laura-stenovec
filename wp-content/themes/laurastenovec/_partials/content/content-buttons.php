<?php



?>

<div class="btns">
	<?php foreach ($content->buttons as $button): ?>
		<a
			href="<?php echo $button['link']['url']; ?>"
			class= "btn btn-primary"
			<?php if(!empty($button['link']['target'])): ?>
				target="<?php echo $button['link']['target']; ?>"
			<?php endif; ?>
		>
			<?php echo $button['link']['title']; ?>
		</a>
	<?php endforeach; ?>
</div>
<?php

	$classes = array();
	$center = false;
	$has_icons = false;
	$has_titles = false;
	$is_link = false;
	$reverse = false;
	$has_bg = ($content->type == 'square');

	if (!empty($content->layout)) {
		$center = in_array('center', $content->layout);
		$classes[] = $center ? 'text-center' : '';

		$has_icons = in_array('icons', $content->layout);
		$has_titles = in_array('titles', $content->layout);
		$is_link = in_array('col-is-link', $content->layout);
		$reverse = in_array('reverse-order', $content->layout);
	}

	if (empty($content->cols_per_row)) {
		$content->cols_per_row = 3;
	}

	if (empty($content->gutter_width)) {
		$content->gutter_width = 'default';
	}

	$classes[] = 'num-cols-'.(!empty($content->cols_per_row) ? $content->cols_per_row : 3);
	$classes[] = 'gutter-width-'.(!empty($content->gutter_width) ? $content->gutter_width : 'default');


?>

<div class="nicdao-columns <?php echo implode(' ', $classes); ?>">
	<div class="row <?php echo $reverse ? 'flex-md-row-reverse' : ''; ?>">
	<?php foreach($content->columns as $i => $column):
		$bg = false;
		$link = false;
		$col_classes = array();

		if ($has_bg) {
			$bg = getImgUrl($column['bg_img'], 'large');
		} 

		if ($is_link) {
			$link = $column['link'];
		}

		if (!empty($column['column_span']) && intval($column['column_span']) > 1) {
			$col_classes[] = 'col-span-'.$column['column_span'];
		}

		if ($content->type == 'numbered' ) {
			if (is_array($column['numbered_styling'])) {
				$col_classes[] = in_array('hide-col-num', $column['numbered_styling']) ? 'hide-col-num' : false;
			}
		}

		?>
		<div class="col <?php echo implode(' ', $col_classes); ?>">

		<?php if($content->type == 'square') {

			$square = new Square($column['title'], $bg, $link);
			if(!empty($column['content'])) {
				$square->content = $column['content'];
			}
			$square->render();

		} else { ?>


			<?php if ($has_icons && !empty($column['icon'])): ?>
				<i class="fa fa-<?php $column['icon']; ?>"></i>
			<?php endif; ?>

			<?php if($content->type == 'numbered'): ?>
				<span class="col-number"><?php echo $i + 1; ?></span>
			<?php endif; ?>

			<?php if ($has_titles && !empty($column['title'])): ?>
				<h3 class="col-title"><?php echo $column['title']; ?></h3>
			<?php endif; ?>

			<div class="content">
				<?php echo $column['content']; ?>
			</div>

		<?php } ?>

		</div>
	<?php endforeach; ?>
	</div>
</div>
<?php

$url = $content->video_url;

if ($content->source == 'vimeo') {
	$step1 = explode('/', $url);
	$video_id = end($step1);
	$embedurl = 'http://player.vimeo.com/video/'.$video_id;
} else if ($content->source == 'youtube') {
	$step1=explode('v=', $url);
	$step2 =explode('&',$step1[1]);
	$video_id = $step2[0];
	$embedurl = 'http://www.youtube.com/embed/'.$video_id;
}

?>

<div class="content-embedded_video">
<?php if ($content->source == 'vimeo'): ?>
	<iframe width="320" height="240" src="<?php echo $embedurl; ?>" webkitallowfullscreen="webkitallowfullscreen" mozallowfullscreen="mozallowfullscreen" allowfullscreen="allowfullscreen" frameborder="0"></iframe>
<?php elseif ($content->source == 'youtube'): ?>
	<iframe width="320" height="240" src="<?php echo $embedurl; ?>" frameborder="0"></iframe>
<?php endif; ?>
</div>

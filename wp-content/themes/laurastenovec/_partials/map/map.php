<div id="map-<?php echo $map->id; ?>" class="leaflet-map" style="height: 400px;"></div>

<script type="text/javascript">
	var map = L.map('map-<?php echo $map->id; ?>').setView([51.505, -0.09], 13);
	var marker = null;

	L.tileLayer(tiles+'{z}/{x}/{y}.png', {
		attribution: '',
		maxZoom: 18
	}).addTo(map);

	map.markers = [];
	<?php foreach ($map->getMarkers() as $marker): ?>
		marker = L.marker([<?php echo $marker['latlng']['lat']; ?>, <?php echo $marker['latlng']['lng']; ?>]);
		marker.bindPopup('<?php echo $marker['content'] ?>');
		map.markers.push(marker);
	<?php endforeach; ?>

	map.markerGroup = L.featureGroup(map.markers).addTo(map);
	map.fitBounds(map.markerGroup.getBounds());

	maps['map-<?php echo $map->id; ?>'] = map;
</script>

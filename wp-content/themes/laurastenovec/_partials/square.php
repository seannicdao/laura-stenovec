<?php
	
	$open_modal = false;
	$has_hover_content = false; 

	if (is_array($square->url)) {
		if ($square->url['target'] == 'modal') {
			$open_modal = true;
		}
	}

	if(!empty($square->content)) {
		$has_hover_content = true;
	}

?>

<div class="square <?php echo implode(' ', $classes_array); ?>"
	<?php echo ($square->img ? 'style="background-image:url('.$square->img.')"' : ''); ?>
	>
	<div>
		<?php if ($has_hover_content): ?>
		<div class="square-content content">
			<div>
				<?php echo $square->content; ?>
			</div>
		</div>
		<?php endif; ?>
		<div>
			<h3 <?php echo($square->url ? '' : 'class="btn btn-default"'); ?>>
				<?php if($square->url): ?>
				<a class="btn btn-default <?php echo $open_modal ? 'open-modal' : ''; ?>" href="<?php echo $open_modal ? '#' : $square->url; ?>">
				<?php endif; ?>
					<?php echo $square->title; ?>
				<?php if($square->url): ?>
				</a>
				<?php if ($open_modal): ?>
				<div class="a-modal-content" style="display: none;">
					<div class="modal-title"><?php echo $square->url['title']; ?></div>
					<div class="modal-content"><?php echo $square->url['content']; ?></div>
					<div class="modal-cta-text"><?php echo $square->url['cta-text']; ?></div>
					<div class="modal-cta-url"><?php echo $square->url['cta-url']; ?></div>
				</div>
				<?php endif; ?>
				<?php endif; ?>
			</h3>
		</div>
	</div>
</div>

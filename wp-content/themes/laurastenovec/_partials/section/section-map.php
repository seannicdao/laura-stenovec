<?php

	$map = new GoogleMap('section-'.$section->index);
	$marker = array(
		'content' => $section->address['address'],
		'latlng' => $section->address,
	);

	$map->addMarker($marker);

?>

<div class="section section-<?php echo $section->section_type ?> <?php echo implode(' ', $classes_array); ?>"
	data-section-index="<?php echo $section->index; ?>"
	>
	<?php $map->render(); ?>
</div>
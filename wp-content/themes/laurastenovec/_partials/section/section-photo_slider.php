<?php



?>

<div class="section section-<?php echo $section->section_type ?> <?php echo implode(' ', $classes_array); ?>"
	data-section-index="<?php echo $section->index; ?>"
	>

	<div class="slick">
		<?php if(!empty($section->images)): foreach($section->images as $image): ?>
			<div>
				<img class="img-fluid" src="<?php echo getImgUrl($image['id']); ?>"/>
			</div>
		<?php endforeach; else: ?>
			<div>
				Whoops, there are no images in this slider!
			</div>
		<?php endif; ?>
	</div>
	
</div>
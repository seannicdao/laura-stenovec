<?php



?>

<div class="section section-<?php echo $section->section_type ?> <?php echo implode(' ', $classes_array); ?>"
	data-section-index="<?php echo $section->index; ?>"
	<?php if ($section->use_background_image): ?> style="background-image: url(<?php echo $section->background_image['sizes']['billboard']; ?>)"<?php endif; ?>
	>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="section-content">
          <?php $section->renderContent(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php



?>

<div class="section section-<?php echo $section->section_type ?> <?php echo implode(' ', $classes_array); ?>"
	<?php if ($section->use_background_image): ?> style="background-image: url(<?php echo $section->background_image['sizes']['billboard']; ?>)"<?php endif; ?>
	>
  <div class="container">
    <div class="row section-content">
      <div class="col-12">
        <?php $section->renderContent(); ?>
      </div>
    </div>
  </div>
</div>
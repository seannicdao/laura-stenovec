<?php


?>

<div class="custom-form">

	<?php if (!empty($_POST['my_form_message'])): ?>
		<div class="form-message" style="color: #ff0000; margin: 0 0 40px;">
			<?php echo $_POST['my_form_message']; ?>
		</div>
	<?php endif; ?>

	<form id="new-account-form" method="POST" action="<?php the_permalink(); ?>">
		<label><strong><em>Account Details</em></strong></label>

		<input type="hidden" name="post-action" value="create-user"/>

		<p>
			<input type="text" name="username" placeholder="Username"
				<?php echo !empty($_POST['username']) ? 'value="'.$_POST['username'].'"' : ''; ?>
				/>
		</p>
		<p>
			<input type="email" name="email" placeholder="Email Address"
				<?php echo !empty($_POST['email']) ? 'value="'.$_POST['email'].'"' : ''; ?>
				/>
		</p>
		<p>
			<input type="password" name="password" placeholder="Choose Password"/>
		</p>
		<p>
			<input type="password" name="password_2" placeholder="Confirm Password"/>
		</p>

		<label><strong><em>Profile Details</em></strong></label>
		<p>
			<input type="text" name="display_name" placeholder="Name"
				<?php echo !empty($_POST['display_name']) ? 'value="'.$_POST['display_name'].'"' : ''; ?>
				/>
		</p>

		<div class="form-message">

		</div>

		<div class="btns">
			<button type="submit" class="btn btn-primary">Create Account</button>
		</div>

	</form>

</div>
<div id="modal-login-content" style="display: none;">

    <div class="modal-title">Login</div>
    <div class="modal-content">

    	<div class="wpcf7">
				<form name="loginform" id="loginform" action="<?php echo site_url(); ?>/wp-admin/admin-ajax.php?action=popup-login" method="post">
					
					<p class="login-username">
						<label for="ls_user_login">Username</label>
						<input type="text" name="ls_user_login" class="input" value="" size="20" placeholder="Username">
					</p>
					<p class="login-password">
						<label for="ls_user_pass">Password</label>
						<input type="password" name="ls_user_password" class="input" value="" size="20" placeholder="Password">
					</p>
					
					<p class="login-remember">
						<label><input name="remember" type="checkbox" id="remember" value="forever"> Remember Me</label><br/>
						<a href="<?php echo wp_lostpassword_url(); ?>">Forgot Password?</a>
					</p>

					<div class="form-messages"></div>

					<input type="hidden" name="redirect_to" value="<?php echo site_url(); ?>">

					<div class="btns">
						<button type="submit" class="btn btn-primary">Sign In</button>
					</div>
					
				</form>

			</div>

    </div>

</div>
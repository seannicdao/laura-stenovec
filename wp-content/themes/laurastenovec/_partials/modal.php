<div id="modal">
	<div>
	<a class="modal-close" href="#"><i class="icomoon-x"></i></a>
		<div>
			<h3><span></span></h3>
			<div class="content"></div>
			<div class="cta">
				<a class="btn btn-primary" href="#"></a>
			</div>
		</div>
	</div>
</div>
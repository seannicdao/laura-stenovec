<?php

	$classes_array = $billboard->classesArray();

?>

<div class="billboard <?php echo $classes_array ? implode (' ',$classes_array) : '';  ?>" <?php if ($billboard->img): ?> style="background-image:url(<?php echo $billboard->img; ?>);" <?php endif; ?>>
	<div>
		<div class="container">
			<div>
				<div class="billboard-content">
					<?php $billboard->renderContent(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

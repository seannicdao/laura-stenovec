(function($){
  $.fn.snInlineEditor = function() {
    this.each( function() {
      var self = $(this),
        id = self.data('id'),
        fieldType = self.data('field-type'),
        editBtn = self.find('.fa-edit'),
        contentPublished =  self.find('.content-published'),
        originalContent = '',
        form = self.find('form'),
        submitBtn = form.find('input[type=submit]'),
        previewBtn = form.find('.btn-preview'),
        editorBtn = form.find('.btn-editor'),
        closeBtn = form.find('.btn-close');

      form.submit(function(e){
        e.preventDefault();
        self.getContent();
        $.ajax({
          url: ajaxurl,
          type: "POST",
          dataType: "json",
          data: {
            action: 'save_inline_editor',
            id: id,
            content: self.getContent(),
          }
        }).done(function( data ) {
          if (data.status == 'success') {
            contentPublished.html(data.content);
            self.removeClass('active preview');
          }
          if (data.status == 'error') {
            alert('Uh oh, this did not save');
          }
        });
      });

      editBtn.click(function(e){
        e.preventDefault();
        self.addClass('active');
        originalContent = self.getContent();
      });

      closeBtn.click(function(e){
        e.preventDefault();
        contentPublished.html(originalContent);
        self.removeClass('active preview');
      });

      previewBtn.click(function(e){
        e.preventDefault();
        contentPublished.html(self.getContent());
        self.addClass('preview');
      });

      editorBtn.click(function(e){
        e.preventDefault();
        self.removeClass('preview');
      });


      self.getContent = function() {
        if (fieldType == 'wp_editor') {
          if (tinyMCE.editors[id].getContent) {
            return tinyMCE.editors[id].getContent();
          }
        } else if (fieldType == 'input') {
          return self.find('.sn-inline-field').val();
        }
        return '';
      }

    });
  }
}(jQuery));
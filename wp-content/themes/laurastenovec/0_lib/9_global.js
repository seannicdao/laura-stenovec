var maps = {};
var tiles = 'http://{s}.tile.openstreetmap.org/';

$(document).ready(function() {
	var $loginModalContent = $('#modal-login-content').detach();
	$('body').addClass('dom-ready');

	$('.content img').wrap('<div class="img-wrap"/>');

	(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
	})(jQuery);

  $('.scroll-to-id').click(function(e){
		e.preventDefault();
		var $target = $(this).attr('href');

		$("html, body").animate({
			scrollTop: $($target).first().offset().top
		});
	});

	$('.open-modal').click(function(e){
		e.preventDefault();

		var $modal = $('#modal');
		var $t = $(this).siblings('.a-modal-content');

		var title = $t.find('.modal-title').html();
		var content = $t.find('.modal-content').html();
		var ctaText = $t.find('.modal-cta-text').length ? $t.find('.modal-cta-text').text() : false;
		var ctaUrl = $t.find('.modal-cta-url').length ? $t.find('.modal-cta-url').text() : false;

		$modal.removeClass('has-cta');

		$modal.find('h3 span').html(title);
		$modal.find('.content').html(content);
		if(ctaText && ctaUrl) {
			$modal.addClass('has-cta')
			$modal.find('.btn').attr('href', ctaUrl).html(ctaText);
		}

		$('body').addClass('modal-in');
	});

	$('.modal-close').click(function(e){
		e.preventDefault();

		$('body').removeClass('modal-in');
		$('body').removeClass('modal-login');
	});

	$('a[href="#login"]').click(function(e){
		e.preventDefault();

		var $modal = $('#modal');
		var $t = $loginModalContent;

		var title = $t.find('.modal-title').html();
		var content = $t.find('.modal-content').html();
		var ctaText = $t.find('.modal-cta-text').length ? $t.find('.modal-cta-text').text() : false;

		$modal.removeClass('has-cta');

		$modal.find('h3 span').html(title);
		$modal.find('.content').html(content);

		$modal.find('form').each(function() {
			var $form = $(this);

			$form.find('input').on('keyup', function(e){
				$form.find('.form-messages').html('');
			});

			$form.submit(function(e){

				e.preventDefault();

				$.ajax({
					url: $form.attr('action'),
					type: $form.attr('method'),
					dataType: 'json',
					data: $form.serializeFormJSON(),
					success: function(data) {
						if (data.status == 'error') {
							$form.find('.form-messages').html('<p>'+data.messages.join()+'</p>');
						} else if (data.status == 'success') {
							window.location = data.redirect_url;
						}
					}
				});
			});
		})

		$('body').addClass('modal-in modal-login');
	});

	$('#user_login').attr( 'placeholder', 'Username' );
	$('#user_pass').attr( 'placeholder', 'Password' );

	$('#navbarSupportedContent').on('hide.bs.collapse', function() {
		$('body').removeClass('nav-open');
	}).on('show.bs.collapse', function() {
		$('body').addClass('nav-open');
	});

	window.addEventListener("scroll", function(event) {
    var top = this.scrollY,
        left =this.scrollX,
        $body = $('body');

    if (top < 100 && $body.hasClass('scrolling')) {
    	$body.removeClass('scrolling');
    }
    if (top > 100 && !$body.hasClass('scrolling')) {
    	$body.addClass('scrolling');
    }
	}, false);

});

function responsiveness() {
	var wH = $(window).height();

	$('.billboard').css('padding-top', $('#header').outerHeight());
}

function isWindow(mySize) {
	var size = window.getComputedStyle(document.body,':before').content;

  return (size == '"'+mySize+'"' || size == mySize);
}

$(window).on('load', function(){
	$('body').addClass('loaded');
	responsiveness();
});
$(window).on('resize', responsiveness);

<?php

$settings = array(
    'isBrewerySite' => false,
);

define('PARTIALS_DIR', get_template_directory().'/_partials');

$pwd = getcwd(); //Save default directory
chdir(get_template_directory()); //Change to template Directory

foreach (glob("0_func/*.php") as $filename) {
	require_once $filename;
}

chdir($pwd);

// Activate Plugins
function run_activate_plugin( $plugin ) {
    $current = get_option( 'active_plugins' );
    $plugin = plugin_basename( trim( $plugin ) );

    if ( !in_array( $plugin, $current ) ) {
        $current[] = $plugin;
        sort( $current );
        do_action( 'activate_plugin', trim( $plugin ) );
        update_option( 'active_plugins', $current );
        do_action( 'activate_' . trim( $plugin ) );
        do_action( 'activated_plugin', trim( $plugin) );
    }

    return null;
}

$plugins = array(
);

foreach ($plugins as $plugin_name => $plugin_path) {
	run_activate_plugin( $plugin_path );
}

/*** Primary Menu ***/
register_nav_menu( 'primary', 'Primary Menu' );
register_nav_menu( 'footer', 'Footer Menu' );

/*** Add Featured Image ***/
add_theme_support( 'post-thumbnails' );

/*** Custom Image Sizes ***/
add_image_size( 'menu-full', 0, 240 );
add_image_size( 'billboard', 1900);
add_image_size( 'square', 600, 600, true);

/*** Enable Page Exceprts ***/
add_action('init', 'enable_page_excerpts');
function enable_page_excerpts() { add_post_type_support('page', 'excerpt'); }

// Google Maps API
add_filter('acf/settings/google_api_key', function () {
    $api_key = '';
    return $api_key;
});

/*** Shortcode for internal links ***/
function add_internal_link( $atts ) {
	extract( shortcode_atts( array(
		'text' => '',
		'url' => '#',
		'target' => ''
	), $atts, 'internal_link' ) );

	if (substr($url, 0, 4) != 'http' && substr($url, 0, 1) != '#') $url = site_url($url);

	return '<a href="' . $url . '" target="'.$target.'">' . $text . '</a>';
} add_shortcode( 'internal_link', 'add_internal_link' );

/*** Custom Post Fields ***/
function add_custom_post_types() {

} add_action('init', 'add_custom_post_types');

// Add Global Settings
if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));    
}

function getSocialLinksHTML() {
    $social_links = array(
        'facebook' => 'fab fa-facebook-f',
        'linkedin' => 'fab fa-linkedin-in',
        'instagram' => 'fab fa-instagram',
    );
    
    $html = '';
    foreach ($social_links as $key => $icon) {
       $html .= '<li>';
       $html .= '<a href="'.get_field($key.'_url','options').'" target="_blank">';
       $html .= '<i class="'.$icon.'"></i>';
       $html .= '</a></li>';
    }
    return "<ul class=\"social-links\">$html</ul>";
}

function create_account_form($args = array()) {
    ob_start();
    
    include PARTIALS_DIR.'/account/form-create-account.php';

    $str = ob_get_contents();
    ob_clean();

    return $str;
}
add_shortcode('create_account_form', 'create_account_form');

function custom_log_in() {
    $return = array(
        'status' => 'error',
        'messages' => array(),
    );

    if (empty($_POST['ls_user_login'])
        || empty($_POST['ls_user_password'])) {
        $return['messages'][] = 'Username and password are required.';
    } else {
        if (!empty($_POST)) {
            $creds['user_login'] = $_POST['ls_user_login'];
            $creds['user_password'] = $_POST['ls_user_password'];
            $creds['remember'] = $_POST['remember'];
            $user = wp_signon( $creds, false );
            if ( !is_wp_error($user) ) {
                $return['status'] = 'success';
                $return['redirect_url'] = $_POST['redirect_to'];
            } else {
                $return['messages'][] = 'Incorrect username or password.';
            }
        }
    }
    

    echo json_encode($return);
    
    die();
} add_action( 'wp_ajax_nopriv_popup-login', 'custom_log_in' );

function already_log_in() {
    $return = array(
        'status' => 'error',
        'messages' => array('You are already logged in. Please log out if you would like to switch accounts.'),
    );

    echo json_encode($return);
    
    die();
} add_action( 'wp_ajax_popup-login', 'already_log_in' );

function my_login_logo_one() { 
?> 
<style type="text/css"> 
body.login div#login h1 a {
    background-image: url(<?php echo get_bloginfo('template_directory').'/0_img/logo.png' ?>);
    width: 300px;
    height: 100px;
    background-size: contain;
    background-position: center center;
} 
#nav {
    display: none;
}

.login {
    background: #ffffff;
}

.login form {
    border: 2px solid #153e50;
    box-shadow: none !important;
}
</style>

<script type="text/javascript">
    window.addEventListener('DOMContentLoaded', function() {
        var link = document.querySelector("#login h1 a");
        link.href = "<?php echo site_url(); ?>";
    }, true);
</script>

<?php 
} add_action( 'login_enqueue_scripts', 'my_login_logo_one');

<?php get_header(); ?>

<div id="page-content" class="section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>404 Error</h1>
        Whoops, this page can't be found!

        <?php wp_link_pages(array(
          'before' => '<div>',
          'after' => '</div>',
          'next_or_number'   => 'next',
          'nextpagelink'     => __( 'Next' ),
          'previouspagelink' => __( 'Previous' ),
        )); ?>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
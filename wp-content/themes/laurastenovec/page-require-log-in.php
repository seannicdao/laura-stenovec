<?php

  /*
  Template Name: Logged-In Users Page
  */

  if(is_user_logged_in()) {
    include 'page.php';
  } else {
    $reg = get_field('registration_page', 'options');
    header('Location: '.get_permalink($reg));
  }

?>

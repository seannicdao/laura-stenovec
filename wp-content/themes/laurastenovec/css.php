<?php
	$pwd = getcwd(); //Save default directory
  chdir(get_template_directory()); //Change to template Directory

	foreach (glob("0_css/*.css") as $filename) {
		echo "<link rel='stylesheet' href='" . get_bloginfo('template_url') . '/' . $filename . "' type='text/css' media='screen' charset='utf-8' />\n";
	}

	chdir($pwd);

	echo "<link rel='stylesheet' href='" . get_bloginfo('template_url') . "/style.css' type='text/css' media='screen' charset='utf-8' />\n";
?>

<?php

if ( $_SERVER['REQUEST_METHOD'] == "POST") {

	$return = array();

  $message = 'An unknown error occured';

  if (!empty($_POST['post-action'])) {

  	if ($_POST['post-action'] == 'create-user') {

  		$return['user_id'] = false;

		  if (!empty($_POST['username'])
		      && !empty($_POST['password'])
		      && !empty($_POST['password_2'])
		      && !empty($_POST['email'])) {
				$data = $_POST;
				$return['data'] = $data;
				if ($data['password'] == $data['password_2']) {
				    $return['user_id'] = wp_create_user( $data['username'], $data['password'], $data['email'] );
				} else {
				    $message = 'Passwords do not match';
				}
				if (is_wp_error($return['user_id'])) {
				    $error = $return['user_id'];
				    $message = reset($error);
				    $message = reset($message);
				    $message = reset($message);
				    $return['user_id'] = false;
				}
			} else {
				$message = 'Please enter username, email, password, and password confirmation.';
			}

		  if ($return['user_id']) {
		      $user_id = $return['user_id'];
		      $user = get_user_by( 'id', $user_id ); 
		      if( $user ) {
		          wp_set_current_user( $user_id, $user->user_login );
		          wp_set_auth_cookie( $user_id );
		          
		          if (!empty($data['display_name'])) {
								wp_update_user( array(
									'ID' => $user_id,
									'display_name' => $data['display_name']
								) );
		          }

		          do_action( 'wp_login', $user->user_login );
		      }
		      header('Location: '.site_url('resources'));
		  }

		  $_POST['my_form_message'] = $message;

		}

	}

}

?>